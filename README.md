# ACDC_Challenge_MERIT 

This is an replication of the following [github code](https://github.com/SLDGroup/MERIT/tree/main) which is based on [MERIT](https://arxiv.org/pdf/2303.16892v1).

### The prediction result:
<p align="center">
<img src="images/Good_pred_MERIT.png" width=50% height=50% 
class="center">
</p>

## Reproducibility steps:
### Recommended environment:

```
Python 3.8
Pytorch 1.11.0
torchvision 0.12.0
```

### Create virtual env by using the following commands

```conda create --prefix conda_env/myenv python="3.8"```

```conda activate conda_env/myenv```

### Install required packages:

```pip install torch==1.11.0+cu113 torchvision==0.12.0+cu113 torchaudio==0.11.0 --extra-index-url https://download.pytorch.org/whl/cu113```

Please use ```pip install -r requirements.txt``` to install the dependencies.

### Preprocess ACDC Dataset:

Run the 'Preprocessed_Data.ipynb' file to get preprocessed data for training the model. 

### Train the model:

```CUDA_VISIBLE_DEVICES=0 python -W ignore train_ACDC.py```

You can change the CUDA_VISIBLE_DEVICES to the GPU you want to use.

### Test the model:

```CUDA_VISIBLE_DEVICES=0 python -W ignore test_ACDC.py```

### Test secret set:

The 'Preprocessed_Data.ipynb' can also preprocess the secret test set and create separate folder for it.

Then run ```CUDA_VISIBLE_DEVICES=0 python -W ignore PredictSeceretTest_ACDC.py```.
